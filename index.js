const TEMPLATE_ID = "user-row"
const LIST_ID = "list"
const BASE_URL = "https://randomuser.me/api"
const RESULT_COUNT = 200
const SEED = "ontegrainternship"
const BTN_DELETE = "btnDelete_"
const BTN_INFO = "btnInfo_";
const USER_DETAILS = "userDetails_"
const QUERY_PARAMS = new URLSearchParams({ results: RESULT_COUNT, seed: SEED })
const DROPDOWN_ID = "infoDrop_"
var btnClicked = false
function showDetails(btnId){
	
	let userId  = btnId.split("_")[1];
	infoDiv = document.getElementById(DROPDOWN_ID.concat(userId))
	if(!btnClicked) {
		btnClicked = true
		infoDiv.style.opacity = "1"
	infoDiv.style.height = "700px"
	}else{
		infoDiv.style.opacity = "0"
		infoDiv.style.height = "0px"
		btnClicked = false
	}
	
	


}
/**
 * Builds a `li` element which will be injected in the `ul`
 * @param {Object} user User to render
 */

function renderUser(user) {
	const template = document.getElementById(TEMPLATE_ID)
	const templateClone = template.content.cloneNode(true)
	
	const templateContainer = templateClone.querySelector("li")
	let userEmail = user.email
	templateContainer.id = userEmail
	// Render image

	let btns = templateContainer.querySelectorAll("button")
	btnInfo = btns[0]
	btnDelete = btns[1]
	btnDelete.id = BTN_DELETE.concat(userEmail)
	btnInfo.id = BTN_INFO.concat(userEmail)
	

	const image = templateContainer.querySelector("img")
	image.src = user.picture.medium
	
	//return a NodeList
	let detailsDiv =  templateContainer.querySelector("#user-details")
	detailsDiv.id = USER_DETAILS.concat(user.email)
	let divFirstName = document.createElement("div")
	let node = document.createTextNode(`First name:${user.name.first}`)
	divFirstName.appendChild(node)
	detailsDiv.appendChild(divFirstName)

	let divLastName = document.createElement("div")
	node = document.createTextNode(`Last name:${user.name.last}`)
	divLastName.appendChild(node)
	detailsDiv.appendChild(divLastName)

	let divEmail = document.createElement("div")
	node = document.createTextNode(`Email : ${user.email}`)
	divEmail.appendChild(node)
	detailsDiv.appendChild(divEmail)

	let divAge = document.createElement("div")
	node = document.createTextNode(`Age : ${user.dob.age}`)
	divAge.appendChild(node)
	detailsDiv.appendChild(divAge)

	//get the dropdown
	let drop = templateContainer.querySelector("#dropdownInfo")
	drop.id = DROPDOWN_ID.concat(user.email)
	return templateClone
}

/**
 * Renders the given list of users in the page
 * @param {Array} users List of users to render
 */
function injectUsersInPage(users) {
	const list = document.getElementById(LIST_ID)
	if (list) {
		
		users.forEach(user => {
			const userElement = renderUser(user)
			list.appendChild(userElement)
			
		})
	}
}

function fetchUsers() {
	fetch(`${BASE_URL}/?${QUERY_PARAMS.toString()}`, { method: "GET" })
		.then(response => response.json())
		.then(response => {
			// TODO Handle response
			console.log(response.results)
			injectUsersInPage(response.results)
		})
		.catch(err => {
			// TODO Handle error
			console.error(err)
		})
		.finally(() => {
			// TODO Handle finally - maybe loading state
			
		})
}


window.addEventListener("load", function () {
	fetchUsers()
})
